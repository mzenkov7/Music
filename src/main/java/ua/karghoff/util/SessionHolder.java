package ua.karghoff.util;

import org.hibernate.Session;

public class SessionHolder {
    private static final ThreadLocal<Session> session = new ThreadLocal<>();

    private SessionHolder() {
    }

    public static Session getSession() {
        return SessionHolder.session.get();
    }

    public static void setSession(Session session) {
        SessionHolder.session.set(session);
    }

    public static void clear() {
        SessionHolder.session.remove();
    }
}
