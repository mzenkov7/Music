package ua.karghoff.rest.impl.json;

import org.apache.log4j.Logger;
import ua.karghoff.dao.TrackDAO;
import ua.karghoff.entity.Track;
import ua.karghoff.rest.TrackController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.servlet.http.HttpServletResponse.*;

@Path("/json/tracks")
public class JSONTrackController implements TrackController {
    private static final Logger LOGGER = Logger.getLogger(JSONTrackController.class);

    private TrackDAO dao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@QueryParam ("page") int page, @QueryParam ("limit") int limit) {
//        LOGGER.info("Get tracks");
        List<Track> tracks = dao.getAll(page, limit);
        if(tracks.size() > 0) {
            return Response.status(SC_OK).entity(tracks).build();
        } else {
            return Response.status(SC_NOT_FOUND).entity("The tracks don't exist.").build();
        }
    }

    @GET
    @Path("/{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Long id) {
//        LOGGER.info("Get track with id: " + id);
        Track track = dao.get(id);
        if(track != null) {
            return Response.status(SC_OK).entity(track).build();
        } else {
            return Response.status(SC_NOT_FOUND).entity("The track with the id " + id + " doesn't exist.").build();
        }
    }

//    @POST
//    @Path("/{id}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response postTrackInJSON(Track track) {
//        String result = "Track posted : " + track;
//        return Response.status(201).entity(result).build();
//    }

    @DELETE
    @Path("/{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
//        LOGGER.info("Deleted track with id: " + id);
        if (dao.remove(id)) {
            return Response.status(SC_NO_CONTENT).entity("Track with id " + id + " have been deleted.").build();
        } else {
            return Response.status(SC_NOT_FOUND).entity("Track with id " + id + " not found.").build();
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAll() {
//        LOGGER.info("Deleted all tracks");
        if (dao.removeAll()) {
            return Response.status(SC_NO_CONTENT).entity("Tracks have been deleted.").build();
        } else {
            return Response.status(SC_NOT_FOUND).entity("Tracks not found.").build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(Track track) {
//        LOGGER.info("Add track : " + track);
        dao.create(track);
        return Response.status(SC_CREATED).build();
    }

    public void setDao(TrackDAO dao) {
        this.dao = dao;
    }
}
