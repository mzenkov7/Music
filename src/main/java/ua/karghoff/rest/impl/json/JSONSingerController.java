package ua.karghoff.rest.impl.json;

import org.apache.log4j.Logger;
import ua.karghoff.dao.SingerDAO;
import ua.karghoff.entity.Singer;
import ua.karghoff.entity.Track;
import ua.karghoff.rest.SingerController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.servlet.http.HttpServletResponse.*;

@Path("/json/singers")
public class JSONSingerController implements SingerController {
    private static final Logger LOGGER = Logger.getLogger(JSONSingerController.class);

    private SingerDAO dao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@QueryParam ("page") int page, @QueryParam ("limit") int limit) {
//        LOGGER.info("Get singers page : " + page + " limit : " + limit);
        List<Singer> singers = dao.getAll(page, limit);
        if(singers.size() > 0) {
            return Response.status(SC_OK).entity(singers).build();
        } else {
            return Response.status(SC_NOT_FOUND).entity("The singers don't exist.").build();
        }
    }

    @GET
    @Path("/{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Long id) {
//        LOGGER.info("Get singer with id: " + id);
        Singer singer = dao.get(id);
        if(singer != null) {
            return Response.status(SC_OK).entity(singer).build();
        } else {
            return Response.status(SC_NOT_FOUND).entity("The singer with the id " + id + " doesn't exist.").build();
        }
    }

    @GET
    @Path("/{id : \\d+}/tracks")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Track> getAllTracks(@PathParam("id") Long id) {
//        LOGGER.info("Get all tracks of singer with id: " + id);
        return dao.get(id).getTracks();
    }

//    @POST
//    @Path("/{id}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response postTrackInJSON(Track track) {
//        String result = "Track posted : " + track;
//        return Response.status(201).entity(result).build();
//    }

    @DELETE
    @Path("/{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
//        LOGGER.info("Deleted singer with id: " + id);
        if (dao.remove(id)) {
            return Response.status(SC_NO_CONTENT).entity("Singer with id " + id + " have been deleted.").build();
        } else {
            return Response.status(SC_NOT_FOUND).entity("Singer with id " + id + " not found.").build();
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAll() {
//        LOGGER.info("Deleted all singers");
        if (dao.removeAll()) {
            return Response.status(SC_NO_CONTENT).entity("Singers have been deleted.").build();
        } else {
            return Response.status(SC_NOT_FOUND).entity("Singers not found.").build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(Singer singer) {
//        LOGGER.info("Add singer : " + singer);
        dao.create(singer);
        return Response.status(SC_CREATED).build();
    }

    public void setDao(SingerDAO dao) {
        this.dao = dao;
    }
}
