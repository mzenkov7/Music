package ua.karghoff.rest;

import ua.karghoff.entity.Track;

import javax.ws.rs.core.Response;
import java.util.List;

public interface TrackController extends Controller<Track> {

    Response put(Track track);
}
