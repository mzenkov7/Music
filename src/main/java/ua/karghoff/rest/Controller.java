package ua.karghoff.rest;

import javax.ws.rs.core.Response;
import java.util.List;

public interface Controller<T> {

    Response getAll(int page, int limit);

    Response get(Long id);

    Response delete(Long id);

    Response deleteAll();

    Response put(T entitiy);
}
