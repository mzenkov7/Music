package ua.karghoff.rest;

import ua.karghoff.entity.Singer;
import ua.karghoff.entity.Track;

import javax.ws.rs.core.Response;
import java.util.List;

public interface SingerController extends Controller<Singer> {

    Response put(Singer singer);
}
