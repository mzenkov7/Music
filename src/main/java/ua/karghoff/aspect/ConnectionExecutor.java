package ua.karghoff.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.hibernate.Session;
import ua.karghoff.dao.impl.AbstractDAO;
import ua.karghoff.exception.DBException;
import ua.karghoff.util.HibernateUtil;
import ua.karghoff.util.SessionHolder;

public class ConnectionExecutor {
    private static final Logger LOG = Logger.getLogger(ConnectionExecutor.class);

    public Object doConnectionExecution(ProceedingJoinPoint point) throws Throwable {
        Session session = HibernateUtil.getSessionFactory().openSession();
        SessionHolder.setSession(session);

        Object returnObj = null;
        try {
            returnObj = point.proceed();
        } catch (Exception ex) {
            LOG.error("Exception during DB request", ex);
            throw new DBException(ex);
        } finally {
            SessionHolder.getSession().close();
            SessionHolder.clear();
        }
        return returnObj;
    }
}
