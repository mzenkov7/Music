package ua.karghoff.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

public class Timer {
    private static final Logger LOG = Logger.getLogger(Timer.class);

    public Object around(ProceedingJoinPoint point) throws Throwable {
        long start = System.currentTimeMillis();
        Object returnObj = point.proceed();
        long end = System.currentTimeMillis();

        LOG.info(
                String.format("#%s(%s() : %s): %d ms",
                        point.getTarget().getClass().getName(),
                        point.getSignature().getName(),
                        returnObj,
                        end - start
                )
        );
        return returnObj;
    }
}
