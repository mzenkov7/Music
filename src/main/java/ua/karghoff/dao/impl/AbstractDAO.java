package ua.karghoff.dao.impl;


import org.hibernate.Query;
import org.hibernate.Session;
import ua.karghoff.annotation.Meterable;
import ua.karghoff.dao.DAO;
import ua.karghoff.exception.DBException;
import ua.karghoff.util.SessionHolder;

import java.util.List;

@Meterable
public abstract class AbstractDAO<T> implements DAO<T> {

    protected abstract Class<T> getEntityClass();
    protected abstract String getEntityName();

    @Override
    public T get(Long id) {
        Session session = SessionHolder.getSession();

        return session.get(getEntityClass(), id);
    }

    @Override
    public boolean remove(Long id) {
        Session session = SessionHolder.getSession();

        String hql = "delete from " + getEntityName() + " where id = :id";
        Query query = session.createQuery(hql).setLong("id", id);
        int rowsAffected = query.executeUpdate();
        return rowsAffected > 0;
    }

    @Override
    public boolean removeAll() {
        Session session = SessionHolder.getSession();

        String hql = "delete from " + getEntityName();
        Query query = session.createQuery(hql);
        int rowsAffected = query.executeUpdate();
        return rowsAffected > 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> getAll(int page, int limit) {
        Session session = SessionHolder.getSession();

        Query query = session.createQuery("from " + getEntityName());
        query.setFirstResult(page * limit);
        if (limit != 0) {
            query.setMaxResults(limit);
        }
        return query.list();
    }

    @Override
    public void create(T entity) {
        Session session = SessionHolder.getSession();

        session.save(entity);
    }

    @Override
    public void update(T entity) {
        Session session = SessionHolder.getSession();

        session.update(entity);
    }
}
