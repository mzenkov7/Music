package ua.karghoff.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import ua.karghoff.dao.TrackDAO;
import ua.karghoff.entity.Track;
import ua.karghoff.util.SessionHolder;

import java.util.List;

public class TrackDAOImpl extends AbstractDAO<Track> implements TrackDAO {

    protected Class<Track> getEntityClass() {
        return Track.class;
    }

    protected String getEntityName() {
        return "Track";
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Track> getTracksBySinger(Long singerId, int page, int limit) {
        Session session = SessionHolder.getSession();

        Query query = session.createQuery("from " + getEntityName() + " where singer.id = :singerId");
        query.setLong("singerId", singerId);
        query.setFirstResult(page * limit);
        if (limit != 0) {
            query.setMaxResults(limit);
        }
        return query.list();
    }
}
