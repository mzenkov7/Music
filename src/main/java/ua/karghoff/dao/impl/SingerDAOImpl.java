package ua.karghoff.dao.impl;

import ua.karghoff.dao.SingerDAO;
import ua.karghoff.entity.Singer;

public class SingerDAOImpl extends AbstractDAO<Singer> implements SingerDAO {

    protected Class<Singer> getEntityClass() {
        return Singer.class;
    }

    protected String getEntityName() {
        return "Singer";
    }
}
