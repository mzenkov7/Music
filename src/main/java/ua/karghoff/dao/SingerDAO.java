package ua.karghoff.dao;

import ua.karghoff.entity.Singer;
import ua.karghoff.entity.Track;

import java.util.List;

public interface SingerDAO extends DAO<Singer> {}
