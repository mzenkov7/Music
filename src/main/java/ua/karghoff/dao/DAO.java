package ua.karghoff.dao;

import java.util.List;

public interface DAO<T> {
    T get(Long id);

    boolean remove(Long id);

    boolean removeAll();

    void create(T entity);

    void update(T entity);

    List<T> getAll(int page, int limit);
}
