package ua.karghoff.dao;

import ua.karghoff.entity.Track;

import java.util.List;

public interface TrackDAO extends DAO<Track> {

    List<Track> getTracksBySinger(Long singerId, int page, int limit);
}
