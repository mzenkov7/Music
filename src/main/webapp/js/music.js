var prefixSinger = '/rest/json/singers';
var prefixTrack = '/rest/json/tracks';

var out = document.getElementById('out');

// Common functions
function printData(data) {
    out.innerHTML = data;
}

function printError(jqXHR) {
    out.innerHTML = jqXHR.status + ' ' + jqXHR.responseText;
}

// Singer functions
function printSingerResult(data) {
    if (Array.isArray(data)) {
        printSingers(data);
    } else {
        printSinger(data);
    }
}

function printSinger(data) {
    out.innerHTML =
        'Singer: ' + '\n' +
        '    id: ' + data.id + '\n' +
        '    name: ' + data.name + '\n' +
        '    birthYear: ' + data.birthYear + '\n'
    ;
}

function printSingers(data) {
    var message = '';
    for (var x = 0; x < data.length; x++) {
        message +=
            'Singer: ' + '\n' +
            '    id: ' + data[x].id + '\n' +
            '    name: ' + data[x].name + '\n' +
            '    birthYear: ' + data[x].birthYear + '\n';
    }
    out.innerHTML = message;
}

// Track functions
function printTrackResult(data) {
    if (Array.isArray(data)) {
        printTracks(data);
    } else {
        printTrack(data);
    }
}

function printTrack(data) {
    out.innerHTML =
        'Id: ' + data.id + '\n' +
        'Title: ' + data.title + '\n' +
        'Singer: ' + '\n' +
        '    id: ' + data.singer.id + '\n' +
        '    name: ' + data.singer.name + '\n' +
        '    birthYear: ' + data.singer.birthYear + '\n'
    ;
}

function printTracks(data) {
    var message = '';
    for (var x = 0; x < data.length; x++) {
        message +=
            'Track: ' + '\n' +
            '    id: ' + data[x].id + '\n' +
            '    title: ' + data[x].title + '\n' +
            '    Singer: ' + '\n' +
            '       id: ' + data[x].singer.id + '\n' +
            '       name: ' + data[x].singer.name + '\n' +
            '       birthYear: ' + data[x].singer.birthYear + '\n'
        ;
    }
    out.innerHTML = message;
}


// REST functions
var RestGetSinger = function () {
    var id = document.getElementById('singerId').value;
    $.ajax({
        type: 'GET',
        url: prefixSinger + '/' + id,
        async: true,
        success: [function (data) {
            printSingerResult(data);
        }],
        error: [function (jqXHR) {
            printError(jqXHR);
        }]
    });
};

var RestGetSingers = function () {
    var page = document.getElementById('singerPage').value;
    var limit = document.getElementById('singerLimit').value;
    $.ajax({
        type: 'GET',
        url: prefixSinger + '?page=' + page + '&limit=' + limit,
        async: true,
        success: [function (data) {
            printSingerResult(data);
        }],
        error: [function (jqXHR) {
            printError(jqXHR);
        }]
    });
};

var RestGetTrack = function () {
    var id = document.getElementById('trackId').value;
    $.ajax({
        type: 'GET',
        url: prefixTrack + '/' + id,
        async: true,
        success: [function (data) {
            printTrackResult(data);
        }],
        error: [function (jqXHR) {
            printError(jqXHR);
        }]
    });
};

var RestGetTracks = function () {
    var page = document.getElementById('trackPage').value;
    var limit = document.getElementById('trackLimit').value;
    $.ajax({
        type: 'GET',
        url: prefixTrack + '?page=' + page + '&limit=' + limit,
        async: true,
        success: [function (data) {
            printTrackResult(data);
        }],
        error: [function (jqXHR) {
            printError(jqXHR);
        }]
    });
};

var RestDeleteSinger = function () {
    var id = document.getElementById('deletedSingerId').value;
    $.ajax({
        type: 'DELETE',
        url: prefixSinger + '/' + id,
        async: true,
        success: [function (data) {
            printData(data);
        }],
        error: [function (jqXHR) {
            printError(jqXHR);
        }]
    });
};

var RestDeleteTrack = function () {
    var id = document.getElementById('deletedTrackId').value;
    $.ajax({
        type: 'DELETE',
        url: prefixTrack + '/' + id,
        async: true,
        success: [function (data) {
            printData(data);
        }],
        error: [function (jqXHR) {
            printError(jqXHR);
        }]
    });
};

var RestPut = function () {
    var JSONObject = {
        'title': 'put title',
        'singer': {name: 'put name', birthYear: 1999},
        'id': 200
    };

    $.ajax({
        type: 'PUT',
        url: prefix,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(JSONObject),
        dataType: 'json',
        async: true,
        success: [function (data) {
            printData(data);
        }],
        error: [function (jqXHR) {
            printError(jqXHR);
        }]
    });
};

var RestPost = function () {
    var JSONObject = {
        'title': 'post title',
        'singer': {name: 'post name', birthYear: 1999},
        'id': 200
    };

    $.ajax({
        type: 'POST',
        url: prefix,
        data: JSON.stringify(JSONObject),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: [function (data) {
            printData(data);
        }],
        error: [function (jqXHR) {
            printError(jqXHR);
        }]
    });
};