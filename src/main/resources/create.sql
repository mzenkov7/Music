DROP TABLE IF EXISTS track;
DROP TABLE IF EXISTS singer;

CREATE TABLE singer (
  id integer unsigned NOT NULL AUTO_INCREMENT,
  name varchar(30) NOT NULL,
  year integer unsigned NOT NULL DEFAULT 0,

  PRIMARY KEY (id),
  UNIQUE KEY (name)
);

INSERT INTO singer (name, year) values('John Lennon', 1940);
INSERT INTO singer (name, year) values('Paul McCartney', 1942);
INSERT INTO singer (name, year) values('George Harrison', 1943);
INSERT INTO singer (name, year) values('Ringo Starr', 1940);
INSERT INTO singer (name, year) values('Rory Storm', 1938);

CREATE TABLE track (
  id integer unsigned NOT NULL AUTO_INCREMENT,
  title varchar(30) NOT NULL,
  singer_id integer unsigned NOT NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (title),
  FOREIGN KEY (singer_id) REFERENCES singer (id) ON DELETE CASCADE
);

INSERT INTO track (title, singer_id) values('Slepping', 1);
INSERT INTO track (title, singer_id) values('Anabel Santa', 2);
INSERT INTO track (title, singer_id) values('Autumn', 3);
INSERT INTO track (title, singer_id) values('Fefesht', 4);